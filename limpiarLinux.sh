#!/bin/bash
# -*- ENCODING: UTF-8 -*-
#
echo 1. Validar cuantos paquetes estan disponibles en la carpeta de cache:
sudo ls /var/cache/pacman/pkg/ | wc -l

echo 2. Verificamos el total de espacio en disco que ocupa la carpeta de cache:
du -sh /var/cache/pacman/pkg/

echo 3. Limpiar todos lo paquetes de cache:
sudo paccache -r

echo 4. Remover paquetes viejos:
sudo paccache -rk 1

echo 5. Remover paquetes de versiones viejos:
sudo paccache -ruk0

echo 6. Remover todos los paquetes intalados o desintalados desde la cache:
sudo pacman -Scc

echo 7. Validar paquetes huerfanos:
sudo pacman -Qtdq

echo 8. Eliminar paquetes huerfanos:
sudo pacman -Rns $(pacman -Qtdq)

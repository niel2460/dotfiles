# Dotfiles

Different configurations of tiling windows managers and other programs:

## Awesome
[Awesome Config](/awesome/)

![](/Captures/awesome.png/)

## Bspwm
[Bspwm config](/bspwm/)

![](/Captures/bspwm.png/)

## Alacritty
[Alacritty config](/alacritty/)

## mpd & ncmpcpp
[mdp config](/mpd/) & [ncmpcpp](/ncmpcpp/)

![](/Captures/ncmpcpp.png/)

## Neofetch
[NeoFetch Config](/neofetch/)

![](/Captures/neofetch.png)

## Polybar
[Polybar Config](/polybar/)

![](/Captures/polybar3.png/)

## Ranger
[Ranger Config](/ranger/)

![](/Captures/ranger.png)

## sxhkd
[Keys Bspwm](/sxhkd/)

## Vim y Neovim
[Vim Config](/nvim/)

![](/Captures/vim.png/)

## Wallpapers
[Wallpapers](https://gitlab.com/niel2460/wallpapapers)

## License
[License](/LICENSE/)

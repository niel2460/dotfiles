#!/bin/bash
# -*- ENCODING: UTF-8 -*-
#
echo
echo ------------------------------
echo Actualizacion del sistema
sudo pacman -Syu

echo
echo -----Instalacion de programas-----

echo
echo --accesorios
sudo pacman -S nitrogen picom neovim lxqt-archiver gnome-calculator keepassxc xreader redshift

echo
echo --Graficos
sudo pacman -S inkscape lximage-qt flameshot

echo
echo --Herramientas del Sistema
sudo pacman -S bleachbit pcmanfm-qt gparted htop ranger w3m ueberzug imlib2 qterminal 

echo
echo --Internet
sudo pacman -S opera opera-ffmpeg-codecs remmina freerdp

echo
echo --Oficina
sudo pacman -S gnucash

echo
echo --Preferencias
sudo pacman -S arandr blueman qt6ct

echo
echo --Programacion
sudo pacman -S code dbeaver

echo
echo --Audio y video
sudo pacman -S strawberry vlc pavucontrol-qt 

echo
echo --Otros complementos 
sudo pacman -S network-manager-applet netctl acpilight tlp acpid scrot xautolock i3lock imagemagick brightnessctl pamixer yarn npm git bluez bluez-utils dunst playerctl yad ntfs-3g gvfs gvfs-afc gvfs-mtp xdg-user-dirs lxqt-powermanagement lxqt-sudo thermald

echo
echo --Temas
sudo pacman -S breeze-gtk breeze-icons papirus-icon-theme capitaine-cursors adapta-gtk-theme

echo 
echo --Windows Manager Bswpm
sudo pacman -S bspwm polybar rofi sxhkd

echo 
echo --Windows Manager I3
sudo pacman -S i3-wm i3status

echo 
echo --Habilitando servicio bluetooth
sudo systemctl enable bluetooth.service

echo 
echo --Habilitando servidor de hora
timedatectl set-timezone America/Tegucigalpa
sudo pacman -Sy ntp
sudo systemctl enable ntpd

echo 
echo --zsh
sudo pacman -S zsh 
chsh -s $(which zsh)
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

echo 
echo --Habiliar AUR
cd
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

echo
echo --Paquetes AUR
yay -S anydesk-bin rustdesk ttf-material-design-icons-webfont 


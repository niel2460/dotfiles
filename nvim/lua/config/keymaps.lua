--vim.g.mapleader = '
--vim.g.maplocalleader = ' '

--funcion para simplificar el establecimiento de teclas
local function map(mode, keys, command, des)
    vim.keymap.set(mode, keys, command, { desc = des }, { silent = true }, { noremap = true })
end

--Explorador de archivos vim
map("n", "<C-o>", vim.cmd.Explore, "Explorador de archivos Vim")
map("n", "<Leader>nt", ':Neotree<CR>', "Explorador de archivos NeoTree")

-- lazy
map("n", "<leader>l", "<cmd>:Lazy<cr>", "Manejador de plugins Lazy")

--compilar vim
map('n', '<Leader>s', ':source<CR>', "Compilar configuraciones vim")

-- Archivo nuevo
map("n", "<leader>fn", "<cmd>enew<cr>", "Nuevo archivo")

--guardar, salir y cerrar pestana
map('n', '<C-s>', ':w<CR>', "Guardar Cambios")
map('n', '<C-q>', ':q<CR>', "Salir de ventana neovim")
map('n', '<C-Q>', ':q!<CR>', "Salir ignorando cambios")
map('n', '<C-w>', ':bdelete<CR>', "Cerrar Ventana")

--copiar y pegar a portapapeles
map({ 'n', 'x' }, '<C-c>', '"+y', "Copiar")
map({ 'n', 'x' }, '<C-v>', '"+p', "Pegar")

--Terminal
map('n', '<Leader>tt', '<CMD>ToggleTerm<CR>', "Terminal Toggle")
map('n', '<leader>tf', '<CMD>ToggleTerm size=13 direction=float<CR>', "Terminal Float")

--navegar por los buffer
map('n', '<TAB>', ':bnext<CR>', "Tag siguiente")
map('n', '<S-TAB>', ':bprev<CR>', "Tag anterior")

--Prettier configuracion para ordenar el codigo
map("n", "<leader>.", vim.lsp.buf.format, "Ordenar codigo con none-ls")

--Configuracion para comentarios
map({ 'n', 'x' }, '<Leader>/', ':Commentary<CR>', "Comentar linea")

--ver ultimos cambios realizados en el archivos
map('n', '<Leader>u', vim.cmd.UndotreeToggle, 'Ver ultimos cambios')

--Busqueda de archivos con telescope
local builtin = require('telescope.builtin')
map('n', '<Leader>ff', builtin.find_files, "Telescope Find File")
map('n', '<Leader>fg', builtin.live_grep, "Telescope Live Grep")
map('n', '<Leader>fb', builtin.buffers, "Telescope Buffers")
map('n', '<Leader>fh', builtin.help_tags, "Telescope Help Tags")

--Lsp
map('n', 'K', vim.lsp.buf.hover, "")
map('n', '<Leader>gd', vim.lsp.buf.definition, "Definicion de variable")
map('n', '<Leader>gr', vim.lsp.buf.references, "")
map({ 'n', 'v' }, '<Leader>ca', vim.lsp.buf.code_action, "Acciones de codigo")

--Desplegar el tagbar
map('n', '<F8>', ':TagbarToggle<CR>', "Tag-Bar")

--dividir ventana
map("n", "<leader>dh", "<CMD>vsplit<CR>", "dividir pantalla horinzontal")
map("n", "<leader>dv", "<CMD>split<CR>", "dividir pantalla vertical")

--Navegacion en ventanas
map("n", "<C-h>", "<C-w>h")
map("n", "<C-l>", "<C-w>l")
map("n", "<C-k>", "<C-w>k")
map("n", "<C-j>", "<C-w>j")

--Redimencionar ventanas
map("n", "<C-Left>", "<C-w><")
map("n", "<C-Right>", "<C-w>>")
map("n", "<C-Up>", "<C-w>+")
map("n", "<C-Down>", "<C-w>-")

-- mover fragmentos de codigo de arriba hacia abajo y viceversa
map("v", "J", ":m '>+1<CR>gv=gv", "Mover seleccion hacia abajo")
map("v", "K", ":m '<-2<CR>gv=gv", "mover seleccion hacia arriba")

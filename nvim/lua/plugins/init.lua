return { -- notificaciones

    -- tema de entorno
    { "catppuccin/nvim",        name = "catppuccin", priority = 1000 },
    { 'folke/tokyonight.nvim' },

    -- iconos web
    { 'ryanoasis/vim-devicons', },

    -- barra de estado
    {
        'vim-airline/vim-airline',
        dependencies = {
            'vim-airline/vim-airline-themes',
        },
        config = function()
            vim.cmd('source ~/.config/nvim/lua/config/airline.vim')
        end
    },

    --plugin para comentar lineas
    { 'tpope/vim-commentary', },

    -- Mostrar colores al usar un codigo de color hexadecimal
    -- {
    --     'norcalli/nvim-colorizer.lua',
    --     config = function()
    --         require'colorizer'.setup()
    --     end
    -- },

    --mostrar los cambios en las lineas de codigo en la barra izquierda cuando se esta en un repositorio git
    { 'mhinz/vim-signify' },

    --servidor de ejecucion html
    -- { 'turbio/bracey.vim',       build = 'npm install --prefix server', },

    --herramientas de autocompletado de llaves, parentesis y completado de etiquetas
    { 'jiangmiao/auto-pairs' },
    { 'windwp/nvim-ts-autotag' },
    { 'alvan/vim-closetag' },
    { 'tpope/vim-surround' },

    --editar multiples lineas de codigo en una seleccion
    { 'mg979/vim-visual-multi',  branch = 'master' },

    -- plugin para ver los ultimos cambios echos en un documento
    { 'mbbill/undotree' },

    --Tagbar barra de exploracion de etiquetas a la derecha
    { 'preservim/tagbar' },

    --Terminal
    { 'akinsho/toggleterm.nvim', version = "*",                         config = true },

    --Plugin para almacenar los atajos de teclado y mostrarlos en pantalla
    {
        'folke/which-key.nvim',
        config = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
            require("which-key").setup {}
        end
    },

    -- Notificaciones
    {
        'rcarriga/nvim-notify',
        config = function()
            vim.notify = require("notify")
        end
    },


}

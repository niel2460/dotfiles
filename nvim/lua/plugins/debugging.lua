return {
    "rcarriga/nvim-dap-ui",
    dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio", },
    config = function()
        local dap, dapui = require("dap"), require("dapui")

        dap.listeners.before.attach.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.launch.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated.dapui_config = function()
            dapui.close()
        end
        dap.listeners.before.event_exited.dapui_config = function()
            dapui.close()
        end

        -- debuggin
        -- vim.keymap.set('n', '<Leader>dt', dap.toggle_breakpoint, { "Debug punto de ruptura" })
        -- vim.keymap.set('n', '<Leader>dc', dap.continue, { "Debug continuar" })
    end,
}

--configuracion
require('config/settings')

--manejador de plugins
require('config/lazy')

--Configuracion de plugins
vim.cmd('source ~/.config/nvim/lua/plugins/airline.vim')
require('plugins/blankline')
require('plugins/colorizer')
require('plugins/lsp')
require('plugins/toggleterm')
require('plugins/treesitter')
require('plugins/which-key')
require('plugins/notify')

--Configuraciones neovim
require('config/colorscheme')
require('config/keymaps')

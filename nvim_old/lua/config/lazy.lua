local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({

    --temas
    { 
        "catppuccin/nvim", name = "catppuccin" 
    },

    --iconos en los elementos visuales
    'ryanoasis/vim-devicons',

    --barra de estado y buffer
    'vim-airline/vim-airline',
    'vim-airline/vim-airline-themes',

    -- LSP Configuracion & Plugins  
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v1.x',
        dependencies = {
            -- LSP Support
            {'neovim/nvim-lspconfig'},             -- Required
            {'williamboman/mason.nvim'},           -- Optional
            {'williamboman/mason-lspconfig.nvim'}, -- Optional

            -- Autocompletion
            {'hrsh7th/nvim-cmp'},         -- Required
            {'hrsh7th/cmp-nvim-lsp'},     -- Required
            {'hrsh7th/cmp-buffer'},       -- Optional
            {'hrsh7th/cmp-path'},         -- Optional
            {'saadparwaiz1/cmp_luasnip'}, -- Optional
            {'hrsh7th/cmp-nvim-lua'},     -- Optional

            -- Snippets
            {'L3MON4D3/LuaSnip'},             -- Required
            {'rafamadriz/friendly-snippets'}, -- Optional
        }
    },

    --Resaltado de sintaxis
    'nvim-treesitter/nvim-treesitter',
    'p00f/nvim-ts-rainbow',
    'nvim-treesitter/nvim-treesitter-refactor',
    'RRethy/nvim-treesitter-textsubjects',

    -- plugin prettier
    {
        'prettier/vim-prettier',
        build = 'yarn install --frozen-lockfile --production',
        --'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'svelte', 'yaml', 'html'],
    },

    --plugin para comentar lineas
    {'tpope/vim-commentary',},

    --Mostrar una linea cuando hay tabulaciones en el codigo
    { "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = {} },

    --Mostrar colores al usar un codigo de color hexadecimal
    'norcalli/nvim-colorizer.lua',

    --mostrar los cambios en las lineas de codigo en la barra izquierda cuando se esta en un repositorio git
    'mhinz/vim-signify',

    --servidor de ejecucion html
    {'turbio/bracey.vim', build = 'npm install --prefix server',},

    --herramientas de autocompletado de llaves, parentesis y completado de etiquetas
    'jiangmiao/auto-pairs',
    'alvan/vim-closetag',
    'tpope/vim-surround',

    --editar multiples lineas de codigo en una seleccion 
    {'mg979/vim-visual-multi', branch = 'master'},

    --Plugin telescope para busqueda de archivo
    {
        'nvim-telescope/telescope.nvim', tag = '0.1.4',
        dependencies = { 'nvim-lua/plenary.nvim' }
    },
    -- plugin para ver los ultimos cambios echos en un documento
    'mbbill/undotree',

    --Tagbar barra de exploracion de etiquetas a la derecha
    'preservim/tagbar',

    --Terminal
    {'akinsho/toggleterm.nvim', version = "*", config = true},

    -- notificaciones
    'rcarriga/nvim-notify',

    --Plugin para almacenar los atajos de teclado y mostrarlos en pantalla
    {
        'folke/which-key.nvim',
        config = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end
    },
})

local set = vim.opt

--opciones de editor
set.number = true
set.relativenumber = true
set.clipboard = "unnamedplus"
set.syntax = "on"
set.encoding = "UTF-8"
set.ruler = true
set.mouse = "a"
set.title = true
set.hidden = true
set.ttimeoutlen = 0
set.wildmenu = true
set.showcmd = true
set.showmatch = true
set.showmode = true
set.cursorline = true

-- opciones de identacion
set.autoindent = true
set.expandtab = true
set.smartindent = true
set.shiftwidth = 4
set.tabstop = 4
set.tw = 2

-- opciones de tipos de letra
set.ignorecase = true
set.smartcase = true

set.termguicolors = true

set.hlsearch = false
set.incsearch = true

-- opciones de respaldo y deshacer cambios
set.swapfile = false
set.backup = false
-- set.undodir = os.getenv("HOME") .. "/.local.share/nvim/undodir"
-- set.undofile = true

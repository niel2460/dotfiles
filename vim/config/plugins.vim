call plug#begin('~/.vim/plugged')

  "Temas
  Plug 'rafi/awesome-vim-colorschemes'
 
  "barra de estado y buffer
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

  "explorador de archivos
  Plug 'scrooloose/nerdtree'

  "iconos en los elementos visuales
  Plug 'ryanoasis/vim-devicons'

  "LSP autocompletado
  Plug 'neovim/nvim-lspconfig'
  Plug 'neoclide/coc.nvim', {'branch': 'master','do':'yarn install --frozen-lockfile'}
 
  "snippets 
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'

  "plugin para mejorar el aspecto visual de las variales
  Plug 'mattn/emmet-vim'
  Plug 'yggdroot/indentline'
  Plug 'prettier/vim-prettier'

  " plugin para comentar lineas
  Plug 'tpope/vim-commentary'

  "mostrar los cambios en las lineas de codigo en la barra izquierda cuando se esta en un repositorio git
  Plug 'mhinz/vim-signify'

  "servidor de ejecucion html
  Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
  Plug 'manzeloth/live-server'

  "herramientas de autocompletado de llaves, parentesis y completado de etiquetas
  Plug 'jiangmiao/auto-pairs'
  Plug 'alvan/vim-closetag'
  Plug 'tpope/vim-surround'
  
  "Plugin telescope para busqueda de archivos
  Plug 'nvim-lua/plenary.nvim'
  Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }

  "Tagbar barra de exploracion a la derecha
  Plug 'preservim/tagbar'

  "Terminal
  Plug 'akinsho/toggleterm.nvim', {'tag' : '*'}
  
  "Plugin para almacenar los atajos de teclado y mostrarlos en pantalla
  Plug 'folke/which-key.nvim'

  "Mostrar colores al usar un codigo de color hexadecimal
  Plug 'norcalli/nvim-colorizer.lua'
  
  "editar multiples lineas de codigo en una seleccion 
  Plug 'mg979/vim-visual-multi', {'branch': 'master'}

call plug#end()


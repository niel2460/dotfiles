let mapleader=" "

"NerdTree
nmap <Leader>nt :NERDTreeToggle<CR>
nmap <Leader>nf :NERDTreeFind<CR>
let NERDTreeQuitOnOpen=1

"guardar, salir y cerrar pestana
nmap <C-s> :w<CR>
nmap <C-q> :q<CR>
nmap <C-w> :bdelete<CR>

"Terminal
map <Leader>tt :ToggleTerm<CR>
map <leader>tf :ToggleTerm size=13 direction=float<CR>

"navegar por las tagbar
nnoremap <silent><TAB> :bnext<CR>
nnoremap <silent><S-TAB> :bprev<CR>

"split resise
nnoremap <Leader>+ 10<C-w>>
nnoremap <Leader>- 10<C-w><

"buffers
map <Leader>ob :Buffers<CR>

"Prettier configuracion para ordenar el codigo
nnoremap <Leader>. :CocCommand prettier.formatFile<CR>

"Configuracion para comentarios
nnoremap <Leader>/ :Commentary<CR>
vnoremap <Leader>/ :Commentary<CR>

"Busqueda de archivos con telescope
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"Desplegar el tagbar
nmap <F8> :TagbarToggle<CR>


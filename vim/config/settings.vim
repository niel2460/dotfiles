"configs"------------------------------------------
syntax enable

set number
set mouse=a
set showcmd
set showmatch
set encoding=UTF-8
" set relativenumber
set sw=2
set noerrorbells
set expandtab
set smartindent
set numberwidth=1
set nowrap
set noswapfile
set nobackup
set incsearch
set ignorecase
set clipboard=unnamedplus
set termguicolors
set cursorline
set ruler
set laststatus=2
set noshowmode
set showtabline=2
set guifont=DroidSansMono_Nerd_Font:h11

" set colorcolumn=120
" highlight colorcolumn ctermbg=0 guibg=lightgrey


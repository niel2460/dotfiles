"Plugins que requieren de lua para inicalizar
lua << EOF
require'colorizer'.setup{'*';}
require'lspconfig'.tsserver.setup{}
require("toggleterm").setup {
  size = 13,
  open_mapping = [[<c-\>]],
  shade_filetypes = {},
  shade_terminals = true,
  shading_factor = '1',
  start_in_insert = true,
  persist_size = true,
  direction = 'horizontal'
}
require("which-key").setup {}
EOF


require'nvim-treesitter.configs'.setup {
     -- A list of parser names, or "all"
    context_commentstring = {
      enable = true,
    },
    ensure_installed = "maintained",
    ensure_installed = {"lua", "vim", "html", "css", "python", "java" },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,
    auto_install = true,
    highlight = {
      enable = true,
    },
}

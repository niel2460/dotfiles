"config plugins"
source ~/.config/nvim/config/plugin_config/airline.vim
source ~/.config/nvim/config/plugin_config/coc.vim
source ~/.config/nvim/config/plugin_config/emmet.vim
source ~/.config/nvim/config/plugin_config/snipets.vim

"settings"
source ~/.config/nvim/config/autoload/plug.vim
source ~/.config/nvim/config/plugins.vim
source ~/.config/nvim/config/keymaps.vim
source ~/.config/nvim/config/colorscheme.vim
source ~/.config/nvim/config/settings.vim
source ~/.config/nvim/config/lua_config.vim

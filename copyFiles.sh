#!/bin/bash
# -*- ENCODING: UTF-8 -*-
#
echo
echo ------------------------------
echo Copiando archivos de configuracion

echo
echo Copiando configuracion de bash y zsh
cp -R .bashrc ~/
cp -R .zshrc ~/

echo
echo Copiando Awesome
cp -R awesome ~/.config/

echo
echo Copiando Bspwm
cp -R bspwm ~/.config/

echo
echo Copiando Dunst
cp -R dunst ~/.config/

echo
echo Copiando I3
cp -R i3 ~/.config/

echo
echo Copiando Neovim 
cp -R nvim ~/.config/

echo
echo Copiando mpd y ncmpcpp 
cp -R mpd ~/.config/
cp -R ncmpcpp ~/.config/

echo
echo Copiando Polybar 
cp -R polybar ~/.config/

echo
echo Copiando Ranger 
cp -R ranger ~/.config/

echo
echo Copiando Redshift 
cp -R redshift ~/.config/

echo
echo Copiando SXHKD 
cp -R sxhkd ~/.config/

echo
echo Copiando scripts de uso general de los window manager
cp -R scriptsWm ~/.config/

echo
echo Copiando fuentes
sudo cp -R fuentes /usr/share/fonts/


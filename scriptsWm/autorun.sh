#!/bin/bash

function run {
   if ! pgrep $1 ;
   then
	$@&
   fi
}

#iniciar dunst para notificaciones
run dunst -conf ~/.config/dunst/dunstrx &

#screens resolution
run xrandr --output HDMI-1 --mode 1600x900 --rate 59.90 --output eDP-1 --mode 1366x768 --rate 60.00 --left-of HDMI-1 

#transparency
run picom

#background screen 1 and 2
run nitrogen --set-zoom-fill --random ~/.config/wallpapers

#network manager applet
run nm-applet 

#power manager
run lxqt-powermanagement

#key num on
run numlockx on

#screensaver
run xautolock -time 5 -locker '~/.config/scriptsWm/screenLock.sh' -notify 30 -notifier '~/.config/scriptsWm/messageLock.sh'

#MEGA
# run megasync

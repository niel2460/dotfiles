#!/bin/bash
#eliminando imagenes de bloqueo existentes
rm /tmp/screen_blur.png
rm /tmp/screen.png
rm /tmp/locking_screen.png

#creando nuevas imagenes de bloqueo
scrot -d 1 /tmp/locking_screen.png
convert -blur 0x8 /tmp/locking_screen.png /tmp/screen_blur.png
convert -composite /tmp/screen_blur.png ~/.config/wallpapers/arch_logo.png -gravity South -geometry -20x200 /tmp/screen.png

#activando el bloque de pantalla
i3lock -i /tmp/screen.png

